This librairy contains functions that helps to work with large fasta file and improve speed and performance when working with large fasta file.
All this librairy is in python only and will stay this way in order to limit dependecy to zero. (I may do a fork with a python wrapper around rust code,
but this directory will remain python only)

New functionality may appears depending of people needs and mine.

Currently it posses two main fonctionnalities, It allows to parse a fasta file with an iterator.
It also implement a fasta indexing function with the corresponding retrieve function. 
Because it use partial indexing it need to sort the fasta file. (But by doing so it manages to outperformed low level code as samtools faidx in term of memeory consumption and speed.)

And So indexing the file can be slow, (~20 minutes for 40_000_000 sequences files) (sorting is slow an python is not great at io anyway).

Once a fasta file is indexed one can retrie a set of specific sequences at lightning speed! (less than a second).
Partial indexing allow to work on extremely large fasta file even on personal computer.

I ended implement it for my PhD project I am working with files of tens of millions of sequences, and this save a lot of time.
It is a work in progress state.
Suggestions, constructive remarks and Issues report are welcome. 

### How to use
FLH is a librairy but It contain two wrapper to be used directly (see use wrapper section)
To use it you should have it in your python path or in the same module as your script.

you can add it temporaly in your script with:

`

    import sys
    sys.path.append("<path of this dir in your systems>")
    import fasta_io


`

#### Use wrapper section
python3 build_index -h

python3 grep_fasta -h

#### Fasta iterator

`

    import fasta_io
    fasta_file = "<path to the fasta file>"

    with open(fasta_file) as f_in:

        for prompt, seq in fasta_io.get_seq_one_by_one(f_in):
            print(">{}\n{}\n".format(prompt, seq)

`

you can also obtain the position in the file in byte of each sequences:
`

    import fasta_io
    fasta_file = "<path to the fasta file>"
    with open(fasta_file) as f_in:
        for prompt, seq, pos in fasta_io.get_seq_one_by_one(f_in, position=True):
            print(">{}\n{}\n".format(prompt, seq)
            
`

the same fonction working in binary exists:

`

    import fasta_io
    fasta_file = "<path to the fasta file>"
    with open(fasta_file, 'rb') as f_in:
        for prompt, seq, pos in fasta_io.get_seq_one_by_one_bytes(f_in, position=True):
            print(">{}\n{}\n".format(prompt.encode(), seq.encode)
`

#### Fasta_indexing
##### Building the index

`

    import fasta_io
    fasta_file = "<path to the fasta file>"
    fasta_io.build_index(fasta_file)

`

You can pass 3 optional args to build_index
 - chunck which define the number of line by temporary file for the sort, (increase it in case of too many file open Error)
 - index_range a trade of by default LFH store the position of every thousand sequences, those key, pair (sequence name, position) are used in a dictionnary if it consume to much memory increase this value
 decrease it may end up with speed up.
 - out_file by default the script produce three file that use the base name of the fasta file pass _indexed.faa (sorted fasta file), indexed_idx (the index), index_idx.pickle  the index in a dictionnary formatted with pickle to be loaded faster.
 changing this option will make the script use the base name (path) provided as new base name for the output.

##### Searching using the index

Now that you indexed your file you can grep sequences from it at lightning speed,
the grep function is an iterator, that take the opened file and a list of ids.
unfound id are return with an "__unfound__" key word as sequence, and won't slow much the search.

`

    import fasta_io
    indexed_fasta_file = "<path to the fasta file>"
    ids= [id0, id1 ... idn]
    with open(indexed_fasta_file) as f_in:
        for prompt, seq in fasta_io.grep_fasta_iter(indexed_fasta_file, ids))
            print(">{}\n{}".format(prompt, seq))
    #### unfound sequences are return like that -> (id, "__unfound__")

`

##### Formating sequences
LFH also come with a small function to split large sequence on multiple ligne of given sizes
seq with line break => chunck_seq(sequence, chunck_size=80)
it is usefull because the fast iterator return the sequence without any line break 
WARNING if your sequence contain line break before they will be considered as character and so won't be remove



### Real data example:

OM-RGC dataset ~40 millions sequences ftp://ftp.sra.ebi.ac.uk/vol1/ERA412/ERA412970/tab/OM-RGC_seq.release.tsv.gz
indexing: 
`

       start =time.time()
       fasta_io.build_index("OM-RGC_seq.release.faa", chunck=1000_000)
       print(time.time() - start )                                               
       862.4323813915253
   

`   
search:
One selected id doesn't exist "OM-RGC.v1.0401548" and you can see, as expected, he barelly have no impact on the indexed search.


`
    
    import fasta_io
    
    id_ = ["OM-RGC.v1.000000001", "OM-RGC.v1.0401548","OM-RGC.v1.030154822", "OM-RGC.v1.010154822"]
    start = time.time()
    with open("OM-RGC_seq.release_indexed.faa") as f_in:
        for prompt, seq in fasta_io.grep_fasta_iter(f_in, id_):
            print(">{}\n{}\n".format(prompt, fasta_io.chunck_seq(seq)))
    
    print(time.time() - start)
    >OM-RGC.v1.0401548
    __unfound__
    
    >OM-RGC.v1.000000001
    to large to show ## on gitlab
    >OM-RGC.v1.010154822
    ITEMTRPLYFPLTIIDGIFGNPEEVLKLAKSLEYQEPAGTNYPGVASKKTIAEIDVDLAKYCCQQIFSPFWDPRDHEIEW
    HVVQDFQKITPHSDRGHLLNNGLIHADNNVGQLGTAIIYLNDAPEGNVGTSFYEKKKDFKSTYLTGSTVPPEYLEATKEY
    HRTGVSNPRLEQLIIEHRSKFNETIRIQQKANRMVLFPAEIWHAQTTYGTGDRYSVRTFVNSANIKQILPSDKSTASTLI
    KSNNVVQARWPMQRTN*
    
    >OM-RGC.v1.030154822
    RKIVYEVSMLDLDEPPKNKMRKVIFKPLTLISKEEKLRIVGELIGRSKRIHEDDIYNCMLEINNNNNKITIAKLAKLLNC
    TSRTIHRNMGVELKREKELLNNQL*

    0.8438785076141357 seconde
    
`

To compare to naive search (worst case because we have a sequence that is not present we must parse all the file):

`

    import fasta_io
    
    id_ = ["OM-RGC.v1.000000001", "OM-RGC.v1.0401548","OM-RGC.v1.030154822", "OM-RGC.v1.010154822"]
    set_id = set(id_)
    start = time.time()
    with open("OM-RGC_seq.release_indexed.faa") as f_in:
        for prompt, seq in fasta_io.get_seq_one_by_one(f_in):
               print(">{}\n{}\n".format(prompt, fasta_io.chunck_seq(seq)))
    
    print(time.time() - start)
    
    >OM-RGC.v1.000000001
    to large to show ## on gitlab
    >OM-RGC.v1.010154822
    ITEMTRPLYFPLTIIDGIFGNPEEVLKLAKSLEYQEPAGTNYPGVASKKTIAEIDVDLAKYCCQQIFSPFWDPRDHEIEW
    HVVQDFQKITPHSDRGHLLNNGLIHADNNVGQLGTAIIYLNDAPEGNVGTSFYEKKKDFKSTYLTGSTVPPEYLEATKEY
    HRTGVSNPRLEQLIIEHRSKFNETIRIQQKANRMVLFPAEIWHAQTTYGTGDRYSVRTFVNSANIKQILPSDKSTASTLI
    KSNNVVQARWPMQRTN*
    
    >OM-RGC.v1.030154822
    RKIVYEVSMLDLDEPPKNKMRKVIFKPLTLISKEEKLRIVGELIGRSKRIHEDDIYNCMLEINNNNNKITIAKLAKLLNC
    TSRTIHRNMGVELKREKELLNNQL*
    
    1014.8242084980011 secondes

`

#### Comparison with samtools
Because samtools do not sort the fasta file indexing is way faster:
`

        /usr/bin/time --verbose samtools faidx OM-RGC_seq.release.faa
        	Command being timed: "samtools faidx OM-RGC_seq.release.faa"
        	User time (seconds): 72.11
`	

`

        /usr/bin/time --verbose samtools faidx  "OM-RGC_seq.release.faa" "OM-RGC.v1.040154822"
        >OM-RGC.v1.040154822
        GPVV*FGLDDALTLRKGTKFSAKVVGSNPIRSTX
        	Command being timed: "samtools faidx OM-RGC_seq.release.faa OM-RGC.v1.040154822"
        	User time (seconds): 32.90
`

Furthermore samtools consumes almost 4 Gb of ram.
