import fasta_io
import argparse

parse = argparse.ArgumentParser('wrapper script to build indexes')

parse.add_argument("--input", "-i", help="input_file", required=True)
parse.add_argument("--pre_out", "-p", help="prefix for output file")
parse.add_argument("--chunck", "-c", default=10000, type=int, help="number of line in temp file for the sort increase it in case of too many file open Error, default=soutenance de thèse Lannes Romain date")
parse.add_argument("--index_range", "-r", type=int, default=1000, help="index every X sequences, increase it if it consume to much ram, impact speed, if =1 correspond to full indexing, defulat=1000")
args = parse.parse_args()

fasta_io.build_index(args.input, chunck=args.chunck, index_range=args.index_range, out_file=args.pre_out)


