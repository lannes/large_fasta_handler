
"""
MIT License

Copyright (c) 2019 Romain Lannes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""


import pickle
import os
import math
import tempfile
import heapq
import sys
import shutil
import time
"""
an open source fasta manager

"""


def chunck_seq(sequence, chunck_size=80):
    new_seq = ""
    cpt = 0
    while cpt <= len(sequence):
        new_seq += sequence[cpt: cpt + chunck_size] + "\n"
        cpt += chunck_size
    return new_seq.strip()


def get_seq_one_by_one_bytes(open_file, position=None):
    """

    An Iterator over an opened fasta file
    :param open_file: an open fasta file
    :return: tuple(prompt, sequence)
    """
    pos = 0
    p, seq = b"", b""
    line = open_file.readline()
    while line:

        if line.startswith(b'>'):

            if seq:

                if not position:
                    yield p, seq
                else:
                    yield p, seq, pos
                p, seq = b"", b""

            p = line[1:].strip()

        else:
            seq += line.strip()
            pos = open_file.tell()
        line = open_file.readline()

    if not position:
        yield p, seq
    else:
        yield p, seq, pos

def get_seq_one_by_one(open_file, position=None):
    """

    An Iterator over an opened fasta file
    :param open_file: an open fasta file
    :return: tuple(prompt, sequence)
    """

    pos = 0
    p, seq = "", ""
    line = open_file.readline()
    while line:

        if line.startswith('>'):

            if seq:

                if not position:
                    yield p, seq
                else:
                    yield p, seq, pos
                p, seq = "", ""

            p = line[1:].strip()

        else:
            seq += line.strip()
            pos = open_file.tell()
        line = open_file.readline()

    if not position:
        yield p, seq
    else:
        yield p, seq, pos


# https://stackoverflow.com/questions/11218477/how-can-i-use-pickle-to-save-a-dict
def __load_pickle_index(index_file):
    with open(index_file, 'rb') as handle:
        b = pickle.load(handle)
    return b


def __make_index_dico_form_index_file(index_file):
    dico = {}
    with open(index_file) as f_in:
        f_in.readline()
        for line in f_in:
            spt = line.strip().split()
            dico[spt[0]] = int(spt[1])
    return dico


def __interval(value, liste):
    if not liste:
        return  0, math.inf
    midle_indices = int(len(liste) / 2)
    borne_supp = len(liste)
    borne_inf = 0

    while True:
        #print("intervals: ", value, midle_indices, liste[midle_indices], liste[midle_indices + 1])
        #print(borne_inf, borne_supp)
        if value < liste[midle_indices]:
            borne_supp = midle_indices
            #midle_indices = int(midle_indices / 2)
        elif value >= liste[midle_indices + 1]:
            borne_inf = midle_indices
            #midle_indices = midle_indices + int((len(liste) - midle_indices) / 2)
        else:
            return midle_indices, midle_indices + 1
        midle_indices = borne_inf + int(((borne_supp - borne_inf) / 2))

        if midle_indices == 0:
            return 0, 1

        elif midle_indices == len(liste) - 1:
            return len(liste) - 2, len(liste) - 1


def __get_index(fasta_file, index_file, index_dict):

    index_dict = {}
    index_dico_file = ""
    index_file_ = ""

    base_name = os.path.splitext(fasta_file.name)[0]
    if index_file or index_dict:
        if index_dict:
            index_dico_file = index_dict
        else:
            index_file_ = index_file

    elif os.path.exists(base_name + ".idx") or os.path.exists(base_name + ".idx.pickle"):

        if os.path.exists(base_name + ".idx.pickle"):
            index_dico_file = base_name + ".idx.pickle"
        else:
            index_file_ = base_name + ".idx"

    if index_dico_file:
        index_dict = __load_pickle_index(index_dico_file)
    elif index_file_:
        index_dict = __make_index_dico_form_index_file(index_file_)

    return index_dict


def grep_fasta_iter(fasta_file, identifier, index_file=None, index_dict=None):

    """
    Will try to use index_file and index_dict if they are not set will try to retrieve them automatically by addin the "_indexed.faa"
    "_indexed.faa.pickle" extension to the fasta file. If it can find index it will not use them.

    :param fasta_file: Open file of the fasta file were to look sequences
    :param identifier: a set or a list of identifier to found
    :param index_file: optional path to the indexed file
    :param index_dict: optional path to the index_dict
    :return: [(p, seq)], unfound identifier are also returend in the form [p, "__unfound__"]
    """
    id_not_found = set()
    # First we need to determine if index exists and load it
    index_dict =__get_index(fasta_file, index_file, index_dict)
    if index_dict:
        index_sorted = sorted(list(index_dict.keys()), key=lambda x: int.from_bytes(x.encode(), "big"))
    else:
        index_sorted = []
    index_bytes = [int.from_bytes(elem.encode(), 'big') for elem in index_sorted]

    # because file are sorted by bytes value we need to sort our identifier the same ways
    list_identifier = sorted(list(identifier), key=lambda x: int.from_bytes(x.encode(), "big"))

    # Those id are use to keep trace of interval where belong the index
    id1, id2 = 0, 1
    for id_to_grep in list_identifier:
        id_to_grep_bytes = int.from_bytes(id_to_grep.encode(), "big")


        if id_to_grep_bytes > index_bytes[id2]:
            id1, id2 = __interval(id_to_grep_bytes, index_bytes)
            fasta_file.seek(index_dict[index_sorted[id1]])

        for prompt, seq, pos in get_seq_one_by_one(fasta_file, position=True):
            if prompt == id_to_grep:
                yield (prompt, seq) # f_out.write()
                fasta_file.seek(pos)
                break

            elif pos > index_dict[index_sorted[id2]]:
                if id2 == len(index_sorted) - 1:
                    continue
                yield (id_to_grep, "__unfound__")
                fasta_file.seek(index_dict[index_sorted[id1]])
                break


def build_index(fasta_file, chunck=10000, index_range=1000, out_file=None):
    """

    :param fasta_file:
    :param chunck:
    :param index_range:
    :param out_file: if outfile the indexed fasta file will take this name and index file and pickle will be set using this name
    :return: the path to the indexed (sorted fasta file)
    """
    base_name = os.path.splitext(fasta_file)[0]
    if out_file:
        indexed_file = out_file
    else:
        indexed_file = base_name + "_indexed.faa"
    start = time.time()

    __main_sort(fasta_file, indexed_file, max_line=chunck)
    print("takes: {} secondes".format(time.time() - start))
    start = time.time()
    print("building index")
    build_index_from_sorted_file(indexed_file, index_range=index_range)
    print("takes: {} secondes".format(time.time() - start))
    return indexed_file


def build_index_from_sorted_file(fasta_file, index_range):

    base_name = os.path.splitext(fasta_file)[0]
    index_file = base_name + ".idx"
    index_pickle = base_name + ".idx.pickle"
    dico = {}
    cpt = 0
    previous_pos = 0
    with open(fasta_file, 'rb') as f_in, open(index_file, "w") as f_out_index:
        f_out_index.write("prompt\tindex\n")

        for p, s, pos in get_seq_one_by_one_bytes(f_in, position=True):
            if cpt % index_range == 0:
                f_out_index.write("{}\t{}\n".format(p.decode(), previous_pos))
                if p.decode() in dico:
                    raise AssertionError("Sequence duplicate name found")
                dico[p.decode()] = previous_pos
            previous_pos = pos
            cpt += 1

    with open(index_pickle, "wb") as handle:
        pickle.dump(dico, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Algorithm based on
# https://github.com/melvilgit/external-Merge-Sort
def __split_large_file(starting_file, my_temp_dir, max_line=1000000):
    """

    :param starting_file: input file to be splitted
    :param my_temp_dir: temporary directory
    :param max_line: number of line to put in each smaller file (ram usage)
    :return: a list with all TemporaryFile
    """
    liste_file = []
    line_holder = []
    cpt = 0
    with open(starting_file, 'rb') as f_in:
        for prompt, seq in get_seq_one_by_one_bytes(f_in):

        #for line in f_in:
            line_holder.append(prompt + b"\t" + seq + b"\n")
            cpt += 1

            if cpt % max_line == 0:
                cpt = 0
                line_holder.sort(key=lambda x: int.from_bytes(x.split(b"\t")[0], 'big'))
                temp_file = tempfile.NamedTemporaryFile(dir=my_temp_dir, delete=False)
                temp_file.writelines(line_holder)
                temp_file.seek(0)
                line_holder = []
                liste_file.append(temp_file)

        if line_holder:
            line_holder.sort(key=lambda x: x.split(b"\t")[0])
            temp_file = tempfile.NamedTemporaryFile(dir=my_temp_dir, delete=False)
            temp_file.writelines(line_holder)
            temp_file.seek(0)
            liste_file.append(temp_file)

    return liste_file


def __merged(liste_file, out_file):
    """

    :param liste_file: a list with all temporary file opened
    :param out_file: the output file
    :param col: the column where to perform the sort, being minimal the script will fail
     if one column is shorter than this value
    :return: path to output file
    """
    my_heap = []
    for elem in liste_file:
        line = elem.readline()
        spt = line.strip().split(b"\t")
        p = spt[0]
        seq = spt[1]
        heapq.heappush(my_heap, [int.from_bytes(p, "big"), elem, p, seq])

    heapq.heapify(my_heap)

    with open(out_file, "w") as out:
        while True:
            minimal = my_heap[0]
            #print(minimal)
            if minimal[0] == math.inf:
                break

            out.write(">{}\n{}\n".format(minimal[2].decode(), chunck_seq(minimal[3].decode())))
            file_temp = minimal[1]

            line = file_temp.readline()
            #print(file_temp, line)
            if not line:
                #print("max")
                #file_name = file_temp.name
                my_heap[0] = [math.inf, None, None]
                #os.remove(file_temp.name)
            else:
                #print("o")
                spt = line.split(b"\t")
                p = spt[0]
                seq = spt[1]
                my_heap[0] = [int.from_bytes(p, "big"), file_temp, p, seq]
                #heapq.heappush(my_heap, [int.from_bytes(p, "big"), elem, p, seq])

            try:
                heapq.heapify(my_heap)
            except:
                for e in my_heap:
                    print(e)
                raise

    return out_file


def __main_sort(big_file, outfile, max_line, tmp_dir=None):

    if not tmp_dir:
        tmp_dir = os.getcwd()

    with tempfile.TemporaryDirectory(dir=tmp_dir) as my_temp_dir:
        temp_dir_file_list = __split_large_file(big_file, my_temp_dir, max_line)
        print("splitted")
        __merged(liste_file=temp_dir_file_list, out_file=outfile)
        del temp_dir_file_list

    print("file merged, sorting done")