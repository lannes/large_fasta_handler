import fasta_io
import argparse

parse = argparse.ArgumentParser()
parse.add_argument("--input", "-i", help="fasta indexed, the .idx file and idx.pickl should be in the same folder", required=True)
parse.add_argument("--identifier", "-d", help="file with identifier to retrive one by line", required=True)
parse.add_argument("--output", "-o", help="output_file", required=True)

args = parse.parse_args()

def read_id(file_):
	l = []
	with open(file_) as f_in:
		for line in f_in:
			l.append(line.strip())
	return l

l = read_id(args.identifier)
with open(args.i) as f_in, open(args.o, 'w') as f_o:
	for p, s in fasta_io.grep_fasta_iter(f_in, l):
		f_o.write(">{}\n{}\n".format(p, fasta_io.chunck_seq(s)))
